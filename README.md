cyberprecios

## Dev requirements:
 - nodejs `sudo apt-get install nodejs`
 - npm `sudo apt-get install npm`
 - vue-cli `sudo npm install -g @vue/cli`
 - vue addon to build the project `npm install -g @vue/cli-service-global`

## Dev flow
Run in local:

    vue serve src/main.js

Generate distribution files on `dist` folder:

    vue build src/main.js
